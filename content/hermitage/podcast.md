---
title: "Sonorités Kabar"
date: 2024-11-30T09:29:16+10:00
weight: 1
background: '/sonoritekabar.jpg'
align: right
---
Dans ce nouveau podcast, la musique réunionnaise est mise à l’honneur. Sauvegarder ce patrimoine immatériel, c’est préserver une richesse culturelle unique. Cette musique raconte l’histoire de l’île, ses luttes, ses joies, ses traditions orales. Un patrimoine un lien entre le passé et le futur qu’il est essentiel de préserver pour ne pas perdre cette mémoire collective.

La Raffinerie est depuis peu située à l’Hermitage. Il nous a semblé naturel de mettre avant des groupes locaux issus du quartier et des environs. Ce choix reflète l’importance de valoriser un lieu qui possède sa propre histoire, ses habitants et ses figures emblématiques, tout en créant un lien fort avec la communauté.

[Tous les épisodes du podcast Sonorités Kabar](https://podcast.tierslieux.re/@sonoriteskabar)

Pour le premier épisode, La Raffinerie met à l’honneur un groupe emblématique de l’Hermitage : Akissassa.
Mais kissa i lé, Akissassa? Jérôme, alias Malbar, nous a accueillis sur sa varangue, lieu où le groupe répète chaque samedi. Depuis plus de 30 ans, Akissassa fait vibrer le quartier, malgré l’absence de local et autre lieu de vie. Lors de cette rencontre exclusive pour Sonorités Kabar, ils ont joué deux morceaux en live. L’épisode commence avec un extrait de « Langaz », titre éponyme de leur cassette sortie à la fin des années 80. Bonne écoute azot !
{{< podcast "https://podcast.tierslieux.re/@sonoriteskabar/episodes/akissassa-30-ans-de-musique-et-dhistoire" >}}

[Retrouvez les autres épisodes du podcast Sonorités Kabar](https://podcast.tierslieux.re/@sonoriteskabar)

Avec la participation de La Région Réunion ![La Région Réunion](/region-reunion-logo.png)
