---
title: "L'Hermitage"
date: 2024-02-12T15:37:57+07:00
heroHeading: "Le patrimoine de L'Hermitage"
heroSubHeading: "L'Hermitage, raconté par ses habitants"
heroBackground: 'ermitage-lontan-crop.jpg'
---
## Pourquoi c'est important ?
Le quartier de L'Hermitage a subi beaucoup de transformation en très peu de temps. Le quartier pêcheur est probablement un des derniers quartiers lontan de la côte ouest. Tous ces changements et ces mélanges sont riches mais vont très vite. Il est important de savoir d'où l'on part : la pratique de pêche, le faire-ensemble, les boutik, les traditions etc.