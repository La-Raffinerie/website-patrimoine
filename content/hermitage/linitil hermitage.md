---
title: "Journal L’initil ! - à L'Hermitage"
weight: 5
date: 2018-12-06T09:29:16+10:00
background: '/linitil/hermitage_optim.jpg'
align: left
#button: 'Contact Us'
#buttonLink: 'contact'
anchor: linitil-hermitage24
---

Journal créé avec les habitants du quartier de L'Hermitage - [Projet L'initil !](projets/Linitil/)

Avec la participation de La Région Réunion ![La Région Réunion](/region-reunion-logo.png)

{{< pdf file="/Linitil/linitil-n3-hermitage_compressed.pdf" >}}

{{< galleries galleryDisplayMode="raws" thumbnailWidth="130" thumbnailHeight="80" >}}
{{< gallery src="/Linitil Hermitage 24/Selection photo linitil hermitage 24-1.jpg"  >}}
{{< gallery src="/Linitil Hermitage 24/Selection photo linitil hermitage 24-2.jpg"  >}}
{{< gallery src="/Linitil Hermitage 24/Selection photo linitil hermitage 24-3.jpg"  >}}
{{< gallery src="/Linitil Hermitage 24/Selection photo linitil hermitage 24-4.png"  >}}
{{< gallery src="/Linitil Hermitage 24/Selection photo linitil hermitage 24-5.png"  >}}
{{< gallery src="/Linitil Hermitage 24/Selection photo linitil hermitage 24-6.png"  >}}
{{< gallery src="/Linitil Hermitage 24/Selection photo linitil hermitage 24-7.png"  >}}
{{< gallery src="/Linitil Hermitage 24/Selection photo linitil hermitage 24-8.JPG"  >}}
{{< gallery src="/Linitil Hermitage 24/Selection photo linitil hermitage 24-9.JPG"  >}}
{{< gallery src="/Linitil Hermitage 24/Selection photo linitil hermitage 24-10.JPG"  >}}
{{< /galleries >}}