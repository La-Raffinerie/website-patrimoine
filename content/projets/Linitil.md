---
title: "L'initil !"
date: 2023-03-18T12:33:46+10:00
draft: false
weight: 3
heroHeading: "L'initil !"
heroSubHeading: 'Journal de bonnes nouvelles du quartier'
heroBackground: 'Linitil/linitil_optim2.jpg'
thumbnail: 'Linitil/linitil.jpg'
---

Arts et Médias : Une équipe atypique s’installe une semaine sur une place publique d’un quartier pour créer, avec les habitants, un journal de bonnes nouvelles du quartier. Des rencontres et récits authentiques, poétiques et ludiques dans un journal «initil» mais essentiel.

Projet réalisé par la compagnie Carton Mécanique, co-produit par La Raffinerie, dans le cadre de résidences hors-les-murs.

«L’Initil !» est un projet hybride mêlant résidence artistique et média innovant, conçu comme un journal de bonnes nouvelles construit en proximité avec les habitant·es. Le journal produit après une semaine de résidence, au gré des rencontres, est axé sur les histoires personnelles, vernaculaires et les récits positifs de la communauté locale.

{{< peertube "https://video.laraffinerie.re/videos/embed/aiR2QqCqcMzTrmHDYLgFbD"  >}}
*teaser vidéo*
 

## Les ressources produites 

{{< ressource page="/savanna/linitil grande fontaine" lien="/savanna" >}}
{{< ressource page="/savanna/linitil savanna" lien="/savanna" >}}
{{< ressource page="/hermitage/linitil hermitage" lien="/hermitage" >}}

