---
title: 'Ernestine Araste'
date: 2018-12-06T09:29:16+10:00
weight: 1
background: '/Ernestine_optim.jpg'
align: right
---

ERNESTINE, DOYENNE DU QUARTIER DE SAVANNA, 96 ANS, PASSIONNÉE PAR LES ROSES - 5 OCTOBRE 2022 - 6 MIN - [Projet C’est Mon Patrimoine 2022](/projets/cmp2022)

Une équipe de nouveaux journalistes âgés de 8 à 10 ans ont parcouru le quartier de Savanna pour réaliser un micro-trottoir sur le patrimoine. Voici la rencontre entre les marmay et la doyenne du quartier. Suite à ce temps d’interview, les marmay ont eu envie de visiter toute la case et la cour d’Ernestine.

{{< podcast "https://podcast.tierslieux.re/@PatrimoineSavanna/episodes/ernestine-doyenne-de-savanna" >}}
