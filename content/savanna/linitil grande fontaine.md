---
title: 'Journal "L’initil !" - à Grande Fontaine'
weight: 6
date: 2018-12-06T09:29:16+10:00
background: '/linitil/grande fontaine_optim.jpg'
align: left
#button: 'Contact Us'
#buttonLink: 'contact'
anchor: linitil-grandefontaine
---

Journal créé avec les habitants du quartier de Grande Fontaine - [Projet L'initil !](projets/Linitil/)


{{< pdf file="/Linitil/Linitil 1 03-03-23 Moulin a eau_.pdf" >}}
