---
title: 'Journal "L’initil !" - à Savanna'
weight: 5
date: 2018-12-06T09:29:16+10:00
background: '/linitil/savanna_optim.jpg'
align: right
#button: 'Contact Us'
#buttonLink: 'contact'
anchor: linitil-savanna
---

Journal créé avec les habitants du quartier de Savanna - [Projet L'initil !](projets/Linitil/)

{{< pdf file="/Linitil/L'initil 2 Savanna 17 mars 2023-R.pdf" >}}
