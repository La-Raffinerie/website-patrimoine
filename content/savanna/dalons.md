---
title: "L'académie des Dalons"
date: 2018-12-06T09:29:16+10:00
weight: 9
background: '/dalons_optim.jpg'
align: left
---

[ Ressource extérieure] L’[Académie des Dalons](https://www.departement974.fr/dalons/) a produit une série d'interviews en 2016 :

Structure à l’initiative du Département.
➭ La finalité du projet est l’insertion sociale et professionnelle en visant l’intégration dans une formation professionnelle.

{{< peertube "https://video.laraffinerie.re/videos/embed/beab12fe-76a2-4bc5-a2c6-63ea03aecec2?warningTitle=0"  >}}
