---
title: 'Savanna'
date: 2018-02-12T15:37:57+07:00
heroHeading: 'Le patrimoine de Savanna'
heroSubHeading: 'Savanna, raconté par ses habitants'
heroBackground: '2560-1901-max_optim.jpg'
---
## Pourquoi c'est important ?
L'usine de Savanna est un lieu symbolique pour les habitants et les quartiers autour. C'est un concentré d'histoire, de culture, de traditions de la vie lontan. C'est aussi un lien très important entre les habitants de toutes les générations. C'est pourquoi nous avons réuni ici les informations collectées sur Savanna.