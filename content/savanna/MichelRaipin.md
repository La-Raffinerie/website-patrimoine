---
title: 'Michel Raïpin Parvedy'
weight: 4
date: 2018-12-06T09:29:16+10:00
background: 'michel_optim.jpg'
align: left
#button: 'Contact Us'
#buttonLink: 'contact'
---
Interview de Michel Raïpin Parvedy réalisée à l’école Emile Hugo, activités périscolaires de l’association Marmay Savanna - 05/10/2022 - [Projet C’est Mon Patrimoine 2022](/projets/cmp2022)

Michel Raïpin Parvedy nous raconte sa famille, le quartier, l’usine sucrière, le temple tamoul. Un mélange de questions travaillées avec les marmay et de questions improvisées. 

{{< podcast "https://podcast.tierslieux.re/@PatrimoineSavanna/episodes/michel-raipin-parvedy-alias-parme" >}}
