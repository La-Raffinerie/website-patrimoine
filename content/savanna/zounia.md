---
title: 'Ginette Zounia'
weight: 3
date: 2018-12-06T09:29:16+10:00
background: 'Zounia_optim.jpg'
align: right
---

RENCONTRE AVEC MADAME GINETTE ZOUNIA - 5 OCTOBRE 2022 - 15 mn - [Projet C’est Mon Patrimoine 2022](/projets/cmp2022)

Après plusieurs ateliers/découverte autour de la captation audiovisuelle et l'exercice de l'interview, les enfants de la garderie "Marmailles Savanna" (École Emile Hugot) ont préparés leurs questions et sont allés à la rencontre des habitants du quartier.
Madame Ginette Zounia, un des gramounes ayant accepté d'échanger avec eux, nous a accueilli chez elle afin d'évoquer ce qu'était "la vie lontan" à proximité de la distillerie de Savanna.

{{< peertube "https://video.laraffinerie.re/videos/embed/0788bb41-1f9c-4a8a-93b7-cf75ceefaaec"  >}}
