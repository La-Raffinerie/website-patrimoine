---
title: 'Daniel Nabo'
weight: 2
date: 2018-12-06T09:29:16+10:00
background: '/danielnabo_optim.jpg'
align: left
#button: 'Contact Us'
#buttonLink: 'contact'
---
Interview de Daniel Nabo réalisée à l’école Emile Hugo, activités périscolaires de l’association Marmay Savanna - 05/10/2022 - [Projet C’est Mon Patrimoine 2022](/projets/cmp2022)

Daniel Nabo, issu de la plus grande famille du quartier, nous raconte sa famille, son enfance, le patrimoine, le quartier. Un mélange de questions travaillées avec les marmay et de questions improvisées.

{{< podcast "https://podcast.tierslieux.re/@PatrimoineSavanna/episodes/daniel-nabo" >}}
