---
title: 'Livre : Distillerie de Savanna 1950-2000'
weight: 8
date: 2018-12-06T09:29:16+10:00
background: 'capture-livre_resized_optim.jpg'
align: right
#button: 'Contact Us'
#buttonLink: 'contact'
---

[ Ressource extérieure] Livre [Distillerie de Savanna – 1950-2000](https://www.la-reunion-des-livres.re/ouvrage/distillerie-de-savanna-1950-2000/) de Bernard LEVENEUR. Groupe Bourbon. Histoire. 2000 

{{< pdf file="/livre-usine_compressed.pdf" >}}
