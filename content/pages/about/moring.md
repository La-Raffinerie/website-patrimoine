---
title: "Moring"
date: 2024-03-30T09:29:16+10:00
weight: 2
background: '/CMP23/moring_optim.JPG'
align: left
anchor: CMP23-Moring
---
Un atelier Moring avec Johan JULIENNE de l’Association Bourant’ - 07/03/24 - [projet CMP2024](projets/cmp2023/)

{{< podcast "https://podcast.tierslieux.re/@lepatrimoinereunionnais/episodes/moring-la-danse-de-combat-avec-johan-julienne" >}}

Johan a expliqué les origines du Moring, les règles, les gestes, le code de conduite et les légendes qui entourent la pratique de cet art de combat. Plusieurs défis ont eu lieu au centre du ron entre les collégiens et les collégiennes au rythme des percussions qui règlent et animent le combat. Il s’agissait d’abord de respecter chaque phase traditionnelle de la rencontre, mais aussi son adversaire !

{{< galleries galleryDisplayMode="raws" thumbnailWidth="130" thumbnailHeight="80" >}}
{{< gallery src="/CMP23/moring/SELECTION_MORING1.jpg"  >}}
{{< gallery src="/CMP23/moring/SELECTION_MORING2.jpg"  >}}
{{< gallery src="/CMP23/moring/SELECTION_MORING3.jpg"  >}}
{{< gallery src="/CMP23/moring/SELECTION_MORING4.jpg"  >}}
{{< gallery src="/CMP23/moring/SELECTION_MORING5.jpg"  >}}
{{< gallery src="/CMP23/moring/SELECTION_MORING6.jpg"  >}}
{{< gallery src="/CMP23/moring/SELECTION_MORING7.jpg"  >}}
{{< gallery src="/CMP23/moring/SELECTION_MORING8.jpg"  >}}
{{< gallery src="/CMP23/moring/SELECTION_MORING9.jpg"  >}}
{{< gallery src="/CMP23/moring/SELECTION_MORING10.jpg"  >}}

{{< /galleries >}}
