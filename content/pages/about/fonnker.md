---
title: "Fonnker"
date: 2024-03-30T09:29:16+10:00
weight: 0
background: '/CMP23/fonnker_optim.JPG'
align: left
anchor: CMP23-Fonnker
---
Un atelier Fonnker avec Kristof LANGROM du [Téat Kabary](https://www.facebook.com/compagnieteatkabary) - 05/03/24 - [Projet CMP2024](projets/cmp2023/)

{{< podcast "https://podcast.tierslieux.re/@lepatrimoinereunionnais/episodes/fonnker-la-poesie-creole-avec-kristof-langrom" >}}

La journée a été consacrée à la découverte du Fonnker. Kristof a expliqué le sens, le rythme de la parole et les rimes, la posture physique et l’intention que l’on doit mettre dans la lecture. Après un atelier d’écriture autour du “premier souvenir”, une lecture commune des différents textes a eu lieu, dans le silence, l’écoute, et le respect de l’autre lorsqu’il prend la parole.


{{< galleries galleryDisplayMode="raws" thumbnailWidth="130" thumbnailHeight="80" >}}
{{< gallery src="/CMP23/fonnker/P1300221.jpg"  >}}
{{< gallery src="/CMP23/fonnker/P1300238.jpg"  >}}
{{< gallery src="/CMP23/fonnker/P1300273.jpg"  >}}
{{< gallery src="/CMP23/fonnker/P1300368.jpg"  >}}
{{< gallery src="/CMP23/fonnker/P1300397.jpg"  >}}
{{< gallery src="/CMP23/fonnker/P1300399.jpg"  >}}
{{< gallery src="/CMP23/fonnker/P1300481.jpg"  >}}
{{< gallery src="/CMP23/fonnker/P1300542.jpg"  >}}
{{< gallery src="/CMP23/fonnker/P1300544.jpg"  >}}
{{< gallery src="/CMP23/fonnker/P1300551.jpg"  >}}
{{< /galleries >}}
