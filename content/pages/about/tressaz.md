---
title: "Tressaz"
date: 2024-03-30T09:29:16+10:00
weight: 3
background: '/CMP23/tressaz_optim.JPG'
align: right
anchor: CMP23-Tressaz
---
Un atelier Tressaz avec Amélie et Timy MOUTOUSSAMY de l’Association les tamarins de l’Ouest- 06/03/24 - [projet CMP2024](projets/cmp2023/)

{{< podcast "https://podcast.tierslieux.re/@lepatrimoinereunionnais/episodes/tressaz-lart-de-la-tresse-avec-timy-moutoussamy" >}}


Cet atelier en deux parties a présenté la sélection des pailles Pikan rouz ou Bourdoné, depuis les techniques d’assouplissement jusqu’à la confection des tresses proprement dites. La tresse est à la base de la confection de chapeaux, objets décoratifs ou de la vie quotidienne, autant de réalisations que l’Association des Tamarins de l’Ouest a amené pour montrer les applications possible de cette pratique traditionnelle. Chacun, à son rythme, a pu patiemment réaliser sa propre tresse.

{{< galleries galleryDisplayMode="raws" thumbnailWidth="130" thumbnailHeight="80" >}}
{{< gallery src="/CMP23/tressaz/SELECTION_TRESSAZ1.jpg"  >}}
{{< gallery src="/CMP23/tressaz/SELECTION_TRESSAZ2.jpg"  >}}
{{< gallery src="/CMP23/tressaz/SELECTION_TRESSAZ3.jpg"  >}}
{{< gallery src="/CMP23/tressaz/SELECTION_TRESSAZ4.jpg"  >}}
{{< gallery src="/CMP23/tressaz/SELECTION_TRESSAZ5.jpg"  >}}
{{< gallery src="/CMP23/tressaz/SELECTION_TRESSAZ6.jpg"  >}}
{{< gallery src="/CMP23/tressaz/SELECTION_TRESSAZ7.jpg"  >}}
{{< gallery src="/CMP23/tressaz/SELECTION_TRESSAZ8.jpg"  >}}
{{< gallery src="/CMP23/tressaz/SELECTION_TRESSAZ9.jpg"  >}}
{{< gallery src="/CMP23/tressaz/SELECTION_TRESSAZ10.jpg"  >}}

{{< /galleries >}}
