---
title: "Maloya"
date: 2024-03-30T09:29:16+10:00
weight: 1
background: '/CMP23/maloya_optim.JPG'
align: right
anchor: CMP23-Maloya
---
Un atelier Maloya avec Gérard CLARA de l’Association Musique et Culture de l’Océan Indien - 07/03/24 - [Projet CMP2024](projets/cmp2023/)

{{< podcast "https://podcast.tierslieux.re/@lepatrimoinereunionnais/episodes/maloya-tradition-ancestrale-avec-gerard-clara" >}}

Après avoir expliqué le rôle des différents instruments à la base du Maloya traditionnel, Gérard a fait découvrir aux collégiens les positions de jeu, les rythmes et les techniques de frappe. Chaque collégien et collégienne a pu pratiquer un instrument, puis des groupes se sont formés autour de la musique, du chant et de la danse. Un délicat travail d’écoute et de synchronisation !

{{< galleries galleryDisplayMode="raws" thumbnailWidth="130" thumbnailHeight="80" >}}
{{< gallery src="/CMP23/maloya/SELECTION_MALOYA1.jpg"  >}}
{{< gallery src="/CMP23/maloya/SELECTION_MALOYA2.jpg"  >}}
{{< gallery src="/CMP23/maloya/SELECTION_MALOYA3.jpg"  >}}
{{< gallery src="/CMP23/maloya/SELECTION_MALOYA4.jpg"  >}}
{{< gallery src="/CMP23/maloya/SELECTION_MALOYA5.jpg"  >}}
{{< gallery src="/CMP23/maloya/SELECTION_MALOYA6.jpg"  >}}
{{< gallery src="/CMP23/maloya/SELECTION_MALOYA7.jpg"  >}}
{{< gallery src="/CMP23/maloya/SELECTION_MALOYA8.jpg"  >}}
{{< gallery src="/CMP23/maloya/SELECTION_MALOYA9.jpg"  >}}
{{< gallery src="/CMP23/maloya/SELECTION_MALOYA10.jpg"  >}}
{{< /galleries >}}
